<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matter extends Model
{
    //
    protected $table = "matter";
    protected $fillable =  array('name',);
}
