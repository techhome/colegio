@extends('layouts.app')
@section('content')
    <h1>
        Materias 
    </h1>
    <a href="{{ route('matter.create') }}" class="btn btn-primary" role="button">Crear Nueva Materia</a>
    <table class="table" style="margin-top:20px;">
            <thead class="thead-dark">
                    <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">accion</th>
                    </tr>
                  </thead>
                  <tbody>
            @foreach($materias as $clave => $valor)
                <tr>
                    <th> {{$valor['nombre']}}</th>
                    <th><a href="{{ route('matter.show',$valor['id'])}}">ver detalles</a></th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection