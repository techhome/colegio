@extends('layouts.app')
@section('content')
    <h1>
        Hijos 
    </h1>
    <a href="{{ route('student.create') }}" class="btn btn-primary" role="button">Crear Nuevo Estudiante</a>
    <table class="table" style="margin-top:20px;">
            <thead class="thead-dark">
                    <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Correo</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
    @foreach($students as $clave => $valor)
        <tr>
            <th> {{$valor['name']}}</th>
            <th>{{$valor['email']}}</th> 
            <th>
                <a href="{{ route('student.edit',$valor['id']) }}" class="btn btn-warning" role="button">editar</a>
                <a href="{{ route('student.show', $valor['id']) }}" class="btn btn-info" role="button">Ver Detalle</a>
                <a href="{{ route('student.destroy', $valor['id']) }}" class="btn btn-danger" role="button">Eliminar</a>
            </th> 
        </tr>
    @endforeach
                  </tbody>
  
    </table>
@endsection