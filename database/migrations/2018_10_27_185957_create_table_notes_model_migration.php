<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotesModelMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->default("registered");
            $table->integer('note')->default(0);
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('assigned_id');
            $table->foreign('student_id')->references('id')->on('student');
            $table->foreign('assigned_id')->references('id')->on('assigned');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
